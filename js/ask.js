(function($){
  var donottrack = null;
  if (window.doNotTrack || navigator.doNotTrack || navigator.msDoNotTrack) {
    if (window.doNotTrack == "1" || navigator.doNotTrack == "yes" || navigator.doNotTrack == "1" || navigator.msDoNotTrack == "1") {
      donottrack = false;
    } else {
      donottrack = false;
    }

  }
  function setStatus(status) {
    $('.cookie-change--status .status').hide();
    $('.cookie-change--status .status-' + status).show();
  }
  var $status = $('.cookie-change--status');
  var getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
    return null;
  };
  var gatracking = getCookie('gatracking');
  console.log('gatracking', gatracking, document.cookie);
  var checkStatus = function() {
    if ($status.length > 0) {
      if (gatracking === '0') {
        setStatus('no');
      }
      else if (gatracking === '1') {
        setStatus('yes');
      }
      else {
        setStatus('unknown');
      }
    }
  }
  var setCookie = function(val){
    var cookieDate = new Date();
    cookieDate.setFullYear(cookieDate.getFullYear() + 1);
    document.cookie = 'gatracking='+val+'; expires=' + cookieDate.toUTCString() + '; SameSite=Lax;';
    gatracking = val;
  };

  $('.cookie-infos .btn-yes').click(function(e){
    e.preventDefault();
    $('.cookie-infos').removeClass('visible');
    setCookie('1');
    checkStatus();
  });
  $('.cookie-infos .btn-no').click(function(e){
    e.preventDefault();
    $('.cookie-infos').removeClass('visible');
    setCookie('0');
    checkStatus();
  });  
  if (gatracking === null && !donottrack) {
    $('.cookie-infos').addClass('visible');
    
    
  }
  
  checkStatus();
  $('.btn-cookie-change').on('click', function(e){
    e.preventDefault();
    $('.cookie-infos').addClass('visible');
  });
  
  
})(jQuery);