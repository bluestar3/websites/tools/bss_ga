<?php declare(strict_types = 1);

namespace Drupal\bss_ga\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a changer la politique de confidentialité block.
 *
 * @Block(
 *   id = "bss_ga_change",
 *   admin_label = @Translation("Changer la politique de confidentialité"),
 *   category = @Translation("Custom"),
 * )
 */
final class ChangeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $conf = \Drupal::config('bss_ga.settings');
    $build['content'] = [
      [
        '#theme' => 'bss_ga_politique',
        '#container' => Html::cleanCssIdentifier($conf->get('container'))
      ],
      '#cache' => ['tags' => ['bss_ga']],
    ];
    return $build;
  }

}
