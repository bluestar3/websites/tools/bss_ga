<?php

namespace Drupal\bss_ga\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bss_ga.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bss_ga_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bss_ga.settings');

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => 'Active',
      '#default_value' => $config->get('active'),
    ];
    $form['ua'] = [
      '#type' => 'textfield',
      '#title' => 'Code de suivi',
      '#required' => FALSE,
      '#default_value' => $config->get('ua'),
    ];
    $default_text = 'Nous utilisons des cookies et des outils d\'analyse pour améliorer la convivialité du site.';
    $def = $config->get('text');
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => 'Texte de la popin',
      '#required' => FALSE,
      '#default_value' => $def ? $def : $default_text,
    ];
    $node = NULL;
    if ($config->get('privacynode')) {
      $node = Node::load($config->get('privacynode'));
    }
    $form['privacynode'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => 'Page de contenu de la politique de confidentialité',
      '#maxlength' => 256,
      '#size' => 64,
      '#default_value' => $node,
    ];
    $default_container = 'container';
    $def = $config->get('container');
    $form['container'] = [
      '#type' => 'textfield',
      '#title' => 'Classe de container',
      '#required' => FALSE,
      '#default_value' => $def ? $def : $default_container,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('bss_ga.settings')
      ->set('ua', $form_state->getValue('ua'))
      ->set('text', $form_state->getValue('text'))
      ->set('privacynode', $form_state->getValue('privacynode'))
      ->set('active', $form_state->getValue('active'))
      ->set('container', $form_state->getValue('container'))
      ->save();
    
    \Drupal\Core\Cache\Cache::invalidateTags(['library_info', 'bss_ga']);
  }

}
